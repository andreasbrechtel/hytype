#!/usr/bin/env python3

import os
import click
import yaml
import marko
from pyquery import PyQuery as pq
import requests
from urllib.parse import urljoin, urlparse

@click.group()
def hytype_cli():
    pass

@hytype_cli.command()
@click.argument('input', type=click.File('r'))
@click.argument('output', type=click.File('w'))
def parse(input,output):
    markdown_input = input.read()
    output.write("<!DOCTYPE html><html><body>\n")
    html_body = marko.convert(markdown_input)
    output.write(html_body)
    output.write("</body></html>\n")

@hytype_cli.command()
@click.argument('input', type=click.File('r'))
@click.argument('output', type=click.File('w'))
def process(input,output):
    html_input = input.read()
    doc = pq(html_input)
    if not doc('head'):
        doc('html').prepend('<head/>')
    head = doc('head')
    for script in doc('script[src]'):
        src_url = urljoin('file://'+os.getcwd()+'/', script.attrib['src'])
        src_url_parsed = urlparse(src_url)
        if src_url_parsed.scheme == 'file':
            with open(src_url_parsed.path,"r") as script_file:
                script_content = script_file.read()
        else:
            raise NotImplementedError("remote URL schemes for script parsing not implemented")
        del script.attrib['src']
        script.text = script_content
    for link_element in doc("link[rel='stylesheet']"):
        href_url = urljoin('file://'+os.getcwd()+'/', link_element.attrib['href'])
        href_url_parsed = urlparse(href_url)
        if href_url_parsed.scheme == 'file':
            with open(href_url_parsed.path,"r") as stylesheet_file:
                stylesheet_content = stylesheet_file.read()
        else:
            raise NotImplementedError("remote URL schemes for script parsing not implemented")
        stylesheet = pq('<style/>')
        stylesheet.text(stylesheet_content)
        head.append(stylesheet)
    doc.remove("link[rel='stylesheet']")

    output.write("<!DOCTYPE html>\n")
    output.write(str(doc))

if __name__ == '__main__':
    hytype_cli(auto_envvar_prefix='HYTYPE')
